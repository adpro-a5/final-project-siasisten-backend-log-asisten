package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LaporanLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.StatusLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service.LaporanLogAsistenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/laporan-log-asisten")
public class LaporanLogAsistenApiController {

    @Autowired
    private LaporanLogAsistenService laporanLogAsistenService;

    @GetMapping(value = "", produces = {"application/json"})
    public ResponseEntity<List<LaporanLogAsisten>> getAllLaporanLogAsistenByIdNpmBulanTahun(
            @RequestParam Map<String, String> requestLaporanAsisten) {
        return ResponseEntity.ok(laporanLogAsistenService.getAllLaporanLogSiasistenByNpmBulanTahun(
                requestLaporanAsisten.get("npmMahasiswa"), requestLaporanAsisten.get("bulanLaporan"),
                Integer.parseInt(requestLaporanAsisten.get("tahunLaporan"))));
    }

    @GetMapping(value = "/{status}", produces = {"application/json"})
    public ResponseEntity<List<LaporanLogAsisten>> getAllLaporanLogAsistenByIdNpmBulanTahunStatus(
            @PathVariable(value = "status") String status,
            @RequestParam Map<String, String> requestLaporanAsisten) {
        return ResponseEntity.ok(laporanLogAsistenService.getAllLaporanLogSiasistenByNpmBulanTahunStatus(
                requestLaporanAsisten.get("npmMahasiswa"), requestLaporanAsisten.get("bulanLaporan"),
                Integer.parseInt(requestLaporanAsisten.get("tahunLaporan")), StatusLogAsisten.valueOf(status)));
    }
}
