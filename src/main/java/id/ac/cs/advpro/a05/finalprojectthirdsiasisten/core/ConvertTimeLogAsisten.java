package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times.ToHours;

import java.util.*;

public class ConvertTimeLogAsisten {

    private final ToHours toHours;

    public ConvertTimeLogAsisten () {
        this.toHours = ToHours.getInstance();
    }

    public Map<String, Integer> getTimes(int jamWaktuMulaiLogAsisten, int menitWaktuMulaiLogAsisten, int detikWaktuMulaiLogAsisten,
                                        int jamWaktuSelesaiLogAsisten, int menitWaktuSelesaiLogAsisten, int detikWaktuSelesaiLogAsisten ) {

        int waktuMulai = getMinutes(jamWaktuMulaiLogAsisten, menitWaktuMulaiLogAsisten, detikWaktuMulaiLogAsisten);
        int waktuSelesai = getMinutes(jamWaktuSelesaiLogAsisten, menitWaktuSelesaiLogAsisten, detikWaktuSelesaiLogAsisten);
        int waktuTotal = waktuSelesai - waktuMulai;

        String time = this.toHours.convert(waktuTotal);
        List<String> hourMinuteSecond = new ArrayList<>(Arrays.asList(time.split("-")));
        Map<String, Integer> times = new HashMap<>();
        times.put("hour", Integer.valueOf(hourMinuteSecond.get(0)));
        times.put("minute", Integer.valueOf(hourMinuteSecond.get(1)));
        times.put("second", Integer.valueOf(hourMinuteSecond.get(2)));

        return times;
    }

    private int getMinutes(int jam, int menit, int detik) {
        return jam*3600 + menit*60 + detik;
    }
}
