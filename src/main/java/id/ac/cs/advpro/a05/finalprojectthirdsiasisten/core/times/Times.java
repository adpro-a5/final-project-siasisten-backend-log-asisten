package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times;

public interface Times {
    String convert(int time);
}
