package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "LogAsisten")
@NoArgsConstructor
public class LogAsisten {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private int id;

    @Column(nullable = false)
    private String npmMahasiswa;

    @Column(nullable = false)
    private String idMataKuliah;

    @Column
    private int tanggalLogAsisten;

    @Column(nullable = false)
    private String bulanLogAsisten;

    @Column
    private int tahunLogAsisten;

    @Column
    private int jamWaktuMulaiLogAsisten;

    @Column
    private int menitWaktuMulaiLogAsisten;

    @Column
    private int detikWaktuMulaiLogAsisten;

    @Column
    private int jamWaktuSelesaiLogAsisten;

    @Column
    private int menitWaktuSelesaiLogAsisten;

    @Column
    private int detikWaktuSelesaiLogAsisten;

    @Column
    private int jamTotalWaktuLogAsisten;

    @Column
    private int menitTotalWaktuLogAsisten;

    @Column
    private int detikTotalWaktuLogAsisten;

    @Column(nullable = false)
    private String kategori;

    @Column(nullable = false)
    private String deskripsiTugas;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusLogAsisten statusLogAsisten;

    public LogAsisten(String idMataKuliah, String npmMahasiswa, int tanggalLogAsisten, String bulanLogAsisten, int tahunLogAsisten,
                      int jamWaktuMulaiLogAsisten, int menitWaktuMulaiLogAsisten, int detikWaktuMulaiLogAsisten,
                      int jamWaktuSelesaiLogAsisten, int menitWaktuSelesaiLogAsisten, int detikWaktuSelesaiLogAsisten,
                      String kategori, String deskripsiTugas) {
        this.idMataKuliah = idMataKuliah;
        this.npmMahasiswa = npmMahasiswa;
        this.tanggalLogAsisten = tanggalLogAsisten;
        this.bulanLogAsisten = bulanLogAsisten;
        this.tahunLogAsisten = tahunLogAsisten;
        this.jamWaktuMulaiLogAsisten = jamWaktuMulaiLogAsisten;
        this.menitWaktuMulaiLogAsisten = menitWaktuMulaiLogAsisten;
        this.detikWaktuMulaiLogAsisten = detikWaktuMulaiLogAsisten;
        this.jamWaktuSelesaiLogAsisten = jamWaktuSelesaiLogAsisten;
        this.menitWaktuSelesaiLogAsisten = menitWaktuSelesaiLogAsisten;
        this.detikWaktuSelesaiLogAsisten = detikWaktuSelesaiLogAsisten;
        this.kategori = kategori;
        this.deskripsiTugas = deskripsiTugas;
        this.statusLogAsisten = StatusLogAsisten.DILAPORKAN;
    }
}
