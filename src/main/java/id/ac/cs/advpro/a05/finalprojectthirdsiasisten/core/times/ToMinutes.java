package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times;

public class ToMinutes implements Times {

    private Times nextTimes;
    private final int denomination;
    private static ToMinutes instance;

    public ToMinutes() {
        this.denomination = 60;
    }

    public static ToMinutes getInstance() {
        if (instance == null) {
            instance = new ToMinutes();
        }
        return instance;
    }

    @Override
    public String convert(int time) {
        String amountOfTime = String.valueOf(time / denomination);
        int reminder = time % denomination;

        if (reminder > 0) {
            amountOfTime = amountOfTime + "-" + this.nextTimes.convert(reminder);
        } else {
            amountOfTime = amountOfTime + "-0";
        }

        return amountOfTime;
    }

    public void setNextTimes(Times nextTimes) {
        this.nextTimes = nextTimes;
    }
}
