package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.ChangeLaporanJam;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LaporanLogAsistenRepository;

public class Create implements ChangeTotalJam {

    private ChangeTotalJam next;
    private final String name;
    private static Create instance;

    public Create() {
        this.name = "create";
    }

    public static Create getInstance() {
        if (instance == null) {
            instance = new Create();
        }
        return instance;
    }

    @Override
    public void changeTotalJam(LogAsisten pastLogAsisten, LogAsisten logAsisten, String action,
                               LaporanLogAsistenRepository laporanLogAsistenRepository) {
        if (this.name.equals((action))) {
            ChangeLaporanJam.getInstance().updateLaporanJam(logAsisten, "next",
                    laporanLogAsistenRepository);
        } else {
            next.changeTotalJam(pastLogAsisten, logAsisten, action, laporanLogAsistenRepository);
        }
    }

    public void setNext(ChangeTotalJam next) {
        this.next = next;
    }
}
