package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "LaporanLogAsisten")
@NoArgsConstructor
public class LaporanLogAsisten {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private int id;

    @Column(nullable = false)
    private String idMataKuliah;

    @Column(nullable = false)
    private String npmMahasiswa;

    @Column(nullable = false)
    private String bulanLaporanLogAsisten;

    @Column
    private int tahunLaporanLogAsisten;

    @Column
    private int honorPerJam;

    @Column
    private double jumlahJam;

    @Column
    private double jumlahPembayaran;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusLogAsisten statusLogAsisten;

    public LaporanLogAsisten(String idMataKuliah, String npmMahasiswa, String bulanLaporanLogAsisten,
                             int tahunLaporanLogAsisten, int honorPerJam, StatusLogAsisten statusLogAsisten) {
        this.idMataKuliah = idMataKuliah;
        this.npmMahasiswa = npmMahasiswa;
        this.bulanLaporanLogAsisten = bulanLaporanLogAsisten;
        this.tahunLaporanLogAsisten = tahunLaporanLogAsisten;
        this.honorPerJam = honorPerJam;
        this.statusLogAsisten = statusLogAsisten;
    }

}
