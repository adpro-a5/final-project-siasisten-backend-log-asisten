package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LaporanLogAsistenRepository;

public interface ChangeTotalJam {
    void changeTotalJam(LogAsisten pastLogAsisten, LogAsisten logAsisten, String action,
                        LaporanLogAsistenRepository laporanLogAsistenRepository);
}
