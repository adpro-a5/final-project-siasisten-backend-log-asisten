package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model;

public enum StatusLogAsisten {
    DILAPORKAN, DISETUJUI, TIDAK_DISETUJUI, DIPROSES, SELESAI
}
