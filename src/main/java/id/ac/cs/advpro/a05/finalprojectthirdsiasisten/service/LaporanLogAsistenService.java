package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LaporanLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.StatusLogAsisten;

import java.util.List;

public interface LaporanLogAsistenService {
    List<LaporanLogAsisten> getAllLaporanLogSiasistenByNpmBulanTahun(String npmMahasiswa, String bulan, int tahun);
    List<LaporanLogAsisten> getAllLaporanLogSiasistenByNpmBulanTahunStatus
            (String npmMahasiswa, String bulan, int tahun, StatusLogAsisten statusLogAsisten);
    void updateLaporanLogAsistenWithLog(LogAsisten pastLogAsisten, LogAsisten logAsisten, String action);
}
