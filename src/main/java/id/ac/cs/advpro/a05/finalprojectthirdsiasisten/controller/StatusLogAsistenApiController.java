package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service.LogAsistenService;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.payload.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/status-log-asisten")
public class StatusLogAsistenApiController {
    @Autowired
    LogAsistenService logAsistenService;

    @PutMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<LogAsisten> updateLogAsisten(@PathVariable(value = "id") int id,
                                                       @RequestBody RequestActionLogAsisten requestActionLogAsisten) {
        return ResponseEntity.ok(logAsistenService.updateStatusLogAsisten(id, requestActionLogAsisten.getStatusLogAsisten()));
    }

    @GetMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<List<LogAsisten>> getAllLogAsistenByIdMataKuliah(@PathVariable(value = "id") String id,
                                                                           @RequestParam Map<String, String> requestLogAsisten) {
        return ResponseEntity.ok(logAsistenService.getAllLogAsistenByIdMataKuliah(id, requestLogAsisten.get("npmMahasiswa")));
    }
}
