package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogAsistenRepository extends JpaRepository<LogAsisten, String> {
    List<LogAsisten> findAllByIdMataKuliahAndNpmMahasiswaOrderByTanggalLogAsistenAscBulanLogAsistenAscTahunLogAsistenAsc
            (String idMataKuliah, String npmMahasiswa);
    List<LogAsisten> findAllByNpmMahasiswaOrderByTanggalLogAsistenAscBulanLogAsistenAscTahunLogAsistenAsc
            (String npmMahasiswa);
    LogAsisten findLogAsistenById(int id);
}
