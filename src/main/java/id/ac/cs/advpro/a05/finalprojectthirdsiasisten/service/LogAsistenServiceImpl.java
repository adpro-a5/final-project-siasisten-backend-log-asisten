package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.ConvertTimeLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.StatusLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LogAsistenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LogAsistenServiceImpl implements LogAsistenService {

    @Autowired
    private LogAsistenRepository logAsistenRepository;

    @Autowired
    private LaporanLogAsistenService laporanLogAsistenService;

    private final ConvertTimeLogAsisten convertTimeLogAsisten;

    public LogAsistenServiceImpl () {
        convertTimeLogAsisten = new ConvertTimeLogAsisten();
    }

    @Override
    public List<LogAsisten> getAllLogAsisten(String npmMahasiswa) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return logAsistenRepository.
                findAllByNpmMahasiswaOrderByTanggalLogAsistenAscBulanLogAsistenAscTahunLogAsistenAsc
                        (npmMahasiswa);
    }

    @Override
    public List<LogAsisten> getAllLogAsistenByIdMataKuliah(String idMataKuliah, String npmMahasiswa) {
        return logAsistenRepository.
                findAllByIdMataKuliahAndNpmMahasiswaOrderByTanggalLogAsistenAscBulanLogAsistenAscTahunLogAsistenAsc
                        (idMataKuliah, npmMahasiswa);
    }

    @Override
    public LogAsisten createLogAsisten(LogAsisten logAsisten) {
        setLogAsisten(logAsisten);
        logAsistenRepository.save(logAsisten);
        laporanLogAsistenService.updateLaporanLogAsistenWithLog(logAsisten, logAsisten, "create");
        return logAsisten;
    }

    @Override
    public LogAsisten getLogAsistenById(int id) {
        return logAsistenRepository.findLogAsistenById(id);
    }

    @Override
    public LogAsisten updateLogAsisten(int id, LogAsisten logAsisten) {
        LogAsisten pastLogAsisten = logAsistenRepository.findLogAsistenById(id);
        setLogAsisten(logAsisten);
        logAsistenRepository.delete(pastLogAsisten);
        logAsistenRepository.save(logAsisten);
        laporanLogAsistenService.updateLaporanLogAsistenWithLog(pastLogAsisten, logAsisten, "update");
        return logAsisten;
    }

    @Override
    public LogAsisten deleteLogAsisten(int id) {
        LogAsisten pastLogAsisten = logAsistenRepository.findLogAsistenById(id);
        logAsistenRepository.delete(pastLogAsisten);
        laporanLogAsistenService.updateLaporanLogAsistenWithLog(pastLogAsisten, pastLogAsisten, "delete");
        return pastLogAsisten;
    }

    @Override
    public LogAsisten updateStatusLogAsisten(int id, String action) {
        LogAsisten pastLog = logAsistenRepository.findLogAsistenById(id);

        if (pastLog != null) {
            LogAsisten newLog = new LogAsisten(pastLog.getIdMataKuliah(), pastLog.getNpmMahasiswa(),
                    pastLog.getTanggalLogAsisten(), pastLog.getBulanLogAsisten(), pastLog.getTahunLogAsisten(),
                    pastLog.getJamWaktuMulaiLogAsisten(), pastLog.getMenitWaktuMulaiLogAsisten(),
                    pastLog.getDetikWaktuMulaiLogAsisten(), pastLog.getJamWaktuSelesaiLogAsisten(),
                    pastLog.getMenitWaktuSelesaiLogAsisten(), pastLog.getDetikWaktuSelesaiLogAsisten(),
                    pastLog.getKategori(), pastLog.getDeskripsiTugas());
            newLog.setJamTotalWaktuLogAsisten(pastLog.getJamTotalWaktuLogAsisten());
            newLog.setMenitTotalWaktuLogAsisten(pastLog.getMenitTotalWaktuLogAsisten());
            newLog.setDetikTotalWaktuLogAsisten(pastLog.getDetikTotalWaktuLogAsisten());
            newLog.setStatusLogAsisten(StatusLogAsisten.valueOf(action));
            logAsistenRepository.delete(pastLog);
            logAsistenRepository.save(newLog);
            laporanLogAsistenService.updateLaporanLogAsistenWithLog(pastLog, newLog, "update");
        }
        return pastLog;
    }

    private void setLogAsisten(LogAsisten logAsisten) {
        Map<String, Integer> times = convertTimeLogAsisten.getTimes(logAsisten.getJamWaktuMulaiLogAsisten(),
                logAsisten.getMenitWaktuMulaiLogAsisten(), logAsisten.getDetikWaktuMulaiLogAsisten(),
                logAsisten.getJamWaktuSelesaiLogAsisten(), logAsisten.getMenitWaktuSelesaiLogAsisten(),
                logAsisten.getDetikWaktuSelesaiLogAsisten());
        logAsisten.setJamTotalWaktuLogAsisten(times.get("hour"));
        logAsisten.setMenitTotalWaktuLogAsisten(times.get("minute"));
        logAsisten.setDetikTotalWaktuLogAsisten(times.get("second"));
        logAsisten.setStatusLogAsisten(StatusLogAsisten.DILAPORKAN);
    }
}
