package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times;

public class ToHours implements Times {

    private Times nextTimes;
    private final int denomination;
    private static ToHours instance;

    public ToHours() {
        this.denomination = 3600;
    }

    public static ToHours getInstance() {
        if (instance == null) {
            instance = new ToHours();
        }
        return instance;
    }

    public void setNextTimes(Times nextTimes) {
        this.nextTimes = nextTimes;
    }

    @Override
    public String convert(int time) {
        String amountOfTime = String.valueOf(time / denomination);
        int reminder = time % denomination;

        if (reminder > 0) {
            amountOfTime = amountOfTime + "-" + this.nextTimes.convert(reminder);
        } else {
            amountOfTime = amountOfTime + "-0-0";
        }

        return amountOfTime;
    }
}
