package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;

import java.util.List;

public interface LogAsistenService {
    List<LogAsisten> getAllLogAsisten(String npmMahasiswa);
    List<LogAsisten> getAllLogAsistenByIdMataKuliah(String idMataKuliah, String npmMahasiswa);
    LogAsisten createLogAsisten(LogAsisten logAsisten);
    LogAsisten getLogAsistenById(int id);
    LogAsisten updateLogAsisten(int id, LogAsisten logAsisten);
    LogAsisten deleteLogAsisten(int id);
    LogAsisten updateStatusLogAsisten(int id, String action);
}
