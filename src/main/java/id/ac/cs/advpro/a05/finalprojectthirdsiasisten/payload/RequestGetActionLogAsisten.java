package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.payload;

import lombok.Data;

@Data
public class RequestGetActionLogAsisten {
    private String npmMahasiswa;
}
