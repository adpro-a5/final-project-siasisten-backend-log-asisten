package id.ac.cs.advpro.a05.finalprojectthirdsiasisten;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten.Create;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten.Delete;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten.Update;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times.ToHours;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times.ToMinutes;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times.ToSeconds;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class FinalProjectThirdSiasistenApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalProjectThirdSiasistenApplication.class, args);
    }

    @PostConstruct
    public void init() {
        ToHours toHours = ToHours.getInstance();
        ToMinutes toMinutes = ToMinutes.getInstance();
        ToSeconds toSeconds = ToSeconds.getInstance();

        toHours.setNextTimes(toMinutes);
        toMinutes.setNextTimes(toSeconds);

        Create create = Create.getInstance();
        Update update = Update.getInstance();
        Delete delete = Delete.getInstance();

        create.setNext(update);
        update.setNext(delete);

    }

}
