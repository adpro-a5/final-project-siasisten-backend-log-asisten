package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LaporanLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.StatusLogAsisten;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LaporanLogAsistenRepository extends JpaRepository<LaporanLogAsisten, String> {
    List<LaporanLogAsisten>
    findAllByNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenOrderByIdMataKuliah
            (String npmMahasiswa, String bulan, int tahun);

    List<LaporanLogAsisten>
    findAllByNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
            (String npmMahasiswa, String bulan, int tahun, StatusLogAsisten statusLogAsisten);

    LaporanLogAsisten
    findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
            (String idMataKuliah, String npmMahasiswa, String bulanLaporan, int tahunLaporan, StatusLogAsisten statusLogAsisten);

}
