package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.ChangeLaporanJam;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LaporanLogAsistenRepository;

public class Delete implements ChangeTotalJam {

    private final String name;
    private static Delete instance;

    public Delete() {
        this.name = "delete";
    }

    public static Delete getInstance() {
        if (instance == null) {
            instance = new Delete();
        }
        return instance;
    }

    @Override
    public void changeTotalJam(LogAsisten pastLogAsisten, LogAsisten logAsisten, String action,
                               LaporanLogAsistenRepository laporanLogAsistenRepository) {
        if (this.name.equals((action))) {
            ChangeLaporanJam.getInstance().updateLaporanJam(pastLogAsisten, "prev",
                    laporanLogAsistenRepository);
        }
    }
}
