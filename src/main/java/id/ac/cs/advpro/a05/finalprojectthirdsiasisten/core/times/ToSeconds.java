package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times;

public class ToSeconds implements Times {

    private final int denomination;
    private static ToSeconds instance;

    public ToSeconds() {
        this.denomination = 1;
    }

    public static ToSeconds getInstance() {
        if (instance == null) {
            instance = new ToSeconds();
        }
        return instance;
    }

    @Override
    public String convert(int time) {
        return String.valueOf(time/denomination);
    }

}
