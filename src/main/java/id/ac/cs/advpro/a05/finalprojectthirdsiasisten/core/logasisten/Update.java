package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.ChangeLaporanJam;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LaporanLogAsistenRepository;

public class Update implements ChangeTotalJam {

    private ChangeTotalJam next;
    private final String name;
    private static Update instance;

    public Update() {
        this.name = "update";
    }

    public static Update getInstance() {
        if (instance == null) {
            instance = new Update();
        }
        return instance;
    }

    @Override
    public void changeTotalJam(LogAsisten pastLogAsisten, LogAsisten logAsisten, String action,
                               LaporanLogAsistenRepository laporanLogAsistenRepository) {
        if (this.name.equals((action))) {
            ChangeLaporanJam.getInstance().updateLaporanJam(pastLogAsisten, "prev",
                    laporanLogAsistenRepository);
            ChangeLaporanJam.getInstance().updateLaporanJam(logAsisten, "next",
                    laporanLogAsistenRepository);
        } else {
            next.changeTotalJam(pastLogAsisten, logAsisten, action, laporanLogAsistenRepository);
        }
    }

    public void setNext(ChangeTotalJam next) {
        this.next = next;
    }
}