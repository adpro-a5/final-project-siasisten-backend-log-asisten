package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten.Create;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LaporanLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.StatusLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LaporanLogAsistenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LaporanLogAsistenServiceImpl implements LaporanLogAsistenService {

    @Autowired
    LaporanLogAsistenRepository laporanLogAsistenRepository;

    private final Create create;

    public LaporanLogAsistenServiceImpl() {
        create = Create.getInstance();
    }

    @Override
    public List<LaporanLogAsisten> getAllLaporanLogSiasistenByNpmBulanTahun
            (String npmMahasiswa, String bulan, int tahun) {

        return laporanLogAsistenRepository.
                findAllByNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenOrderByIdMataKuliah
                        (npmMahasiswa, bulan, tahun);

    }

    @Override
    public List<LaporanLogAsisten> getAllLaporanLogSiasistenByNpmBulanTahunStatus
            (String npmMahasiswa, String bulan, int tahun, StatusLogAsisten statusLogAsisten) {
        return laporanLogAsistenRepository.
                findAllByNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                        (npmMahasiswa, bulan, tahun, statusLogAsisten);
    }

    @Override
    public void updateLaporanLogAsistenWithLog(LogAsisten pastLogAsisten, LogAsisten logAsisten, String action) {
        String idMataKuliah = logAsisten.getIdMataKuliah();
        String npmMahasiswa = logAsisten.getNpmMahasiswa();
        String bulanLogAsisten = logAsisten.getBulanLogAsisten();
        int tahunLogAsisten = logAsisten.getTahunLogAsisten();

        if (laporanLogAsistenRepository.findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                (idMataKuliah, npmMahasiswa, bulanLogAsisten, tahunLogAsisten, StatusLogAsisten.DILAPORKAN) == null)
            createLaporanLogAsisten(idMataKuliah, npmMahasiswa, bulanLogAsisten, tahunLogAsisten);

        create.changeTotalJam(pastLogAsisten, logAsisten, action, laporanLogAsistenRepository);

    }

    public void createLaporanLogAsisten(String id, String npm, String bulan, int tahun) {
        laporanLogAsistenRepository.save(new LaporanLogAsisten(id, npm,
                bulan, tahun, 25000, StatusLogAsisten.DILAPORKAN));
        laporanLogAsistenRepository.save(new LaporanLogAsisten(id, npm,
                bulan, tahun, 25000, StatusLogAsisten.TIDAK_DISETUJUI));
        laporanLogAsistenRepository.save(new LaporanLogAsisten(id, npm,
                bulan, tahun, 25000, StatusLogAsisten.DISETUJUI));
        laporanLogAsistenRepository.save(new LaporanLogAsisten(id, npm,
                bulan, tahun, 25000, StatusLogAsisten.DIPROSES));
        laporanLogAsistenRepository.save(new LaporanLogAsisten(id, npm,
                bulan, tahun, 25000, StatusLogAsisten.SELESAI));
    }

}
