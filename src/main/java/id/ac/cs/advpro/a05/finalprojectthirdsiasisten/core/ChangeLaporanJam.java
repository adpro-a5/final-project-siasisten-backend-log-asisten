package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LaporanLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LaporanLogAsistenRepository;

public class ChangeLaporanJam {

    private static ChangeLaporanJam instance;

    public static ChangeLaporanJam getInstance() {
        if (instance == null) {
            instance = new ChangeLaporanJam();
        }
        return instance;
    }

    public void updateLaporanJam(LogAsisten logAsisten, String action, LaporanLogAsistenRepository laporanLogAsistenRepository) {
        LaporanLogAsisten laporan =
                laporanLogAsistenRepository.
                        findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                                (logAsisten.getIdMataKuliah(), logAsisten.getNpmMahasiswa(), logAsisten.getBulanLogAsisten(),
                                        logAsisten.getTahunLogAsisten(), logAsisten.getStatusLogAsisten());
        double jumlahJam;
        if (action.equals("prev")) {
            jumlahJam = laporan.getJumlahJam() - (
                    logAsisten.getJamTotalWaktuLogAsisten() -
                            (double) logAsisten.getMenitTotalWaktuLogAsisten() / 60 +
                            (double) logAsisten.getDetikTotalWaktuLogAsisten() / 3600);
        } else {
            jumlahJam = laporan.getJumlahJam() + (
                    logAsisten.getJamTotalWaktuLogAsisten() +
                            (double) logAsisten.getMenitTotalWaktuLogAsisten()/60 +
                            (double) logAsisten.getDetikTotalWaktuLogAsisten()/3600);
        }
        laporan.setJumlahJam(jumlahJam);
        laporan.setJumlahPembayaran(jumlahJam*laporan.getHonorPerJam());
        laporanLogAsistenRepository.save(laporan);
    }
}
