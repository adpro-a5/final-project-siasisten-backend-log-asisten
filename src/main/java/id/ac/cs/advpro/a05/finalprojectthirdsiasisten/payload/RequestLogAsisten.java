package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.payload;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import lombok.Data;

@Data
public class RequestLogAsisten {
    private LogAsisten logAsisten;
}
