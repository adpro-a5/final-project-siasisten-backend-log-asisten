package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.payload.RequestLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service.LogAsistenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/log-asisten")
public class LogAsistenApiController {

    @Autowired
    private LogAsistenService logAsistenService;

    @GetMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<LogAsisten> getLogAsistenById(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok(logAsistenService.getLogAsistenById(id));
    }

    @PostMapping(value = "", produces = {"application/json"})
    public ResponseEntity<LogAsisten> createLogAsisten(@RequestBody RequestLogAsisten logAsistenDTO) {
        return ResponseEntity.ok(logAsistenService.createLogAsisten(logAsistenDTO.getLogAsisten()));
    }

    @PutMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<LogAsisten> updateLogAsisten(@PathVariable(value = "id") int id,
                                                       @RequestBody RequestLogAsisten logAsistenDTO) {
        return ResponseEntity.ok(logAsistenService.updateLogAsisten(id, logAsistenDTO.getLogAsisten()));
    }

    @DeleteMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<LogAsisten> deleteLogAsisten(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok(logAsistenService.deleteLogAsisten(id));
    }
}
