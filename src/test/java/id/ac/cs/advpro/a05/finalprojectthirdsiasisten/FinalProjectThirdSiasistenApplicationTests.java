package id.ac.cs.advpro.a05.finalprojectthirdsiasisten;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class FinalProjectThirdSiasistenApplicationTests {

    @Test
    void contextLoads() {
        FinalProjectThirdSiasistenApplication.main(new String[0]);
        assertTrue(true);
    }

}
