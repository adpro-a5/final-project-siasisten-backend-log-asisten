package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.payload.RequestLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service.LogAsistenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LogAsistenApiController.class)
class LogAsistenApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LogAsistenService logAsistenService;

    private RequestLogAsisten logAsistenDTO1;
    private RequestLogAsisten logAsistenDTO2;
    private LogAsisten logAsisten1;
    private LogAsisten logAsisten2;

    @BeforeEach
    void setUp() {
        logAsisten1 = new LogAsisten("0", "0", 24, "Oktober",
                2021, 0, 0, 0,
                1, 1, 1, "Asistensi",
                "UTS");
        logAsisten1.setId(0);
        logAsisten2 = new LogAsisten("1", "0", 24, "Oktober",
                2021, 0, 0, 0,
                1, 0, 0, "Asistensi",
                "UTS");
        logAsisten2.setId(1);

        logAsistenDTO1 = new RequestLogAsisten();
        logAsistenDTO1.setLogAsisten(logAsisten1);
        logAsistenDTO2 = new RequestLogAsisten();
        logAsistenDTO2.setLogAsisten(logAsisten2);
    }

    @Test
    void testGetLogAsistenById() throws Exception {
        when(logAsistenService.getLogAsistenById(0)).thenReturn(logAsisten1);

        mockMvc.perform(get("/api/log-asisten/0")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tanggalLogAsisten").value(24))
                .andExpect(jsonPath("$.bulanLogAsisten").value("Oktober"))
                .andExpect(jsonPath("$.tahunLogAsisten").value(2021));
    }

    @Test
    void testCreateLogAsisten() throws Exception {
        when(logAsistenService.createLogAsisten(logAsisten1)).thenReturn(logAsisten1);

        mockMvc.perform(post("/api/log-asisten")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(logAsistenDTO1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tanggalLogAsisten").value(24))
                .andExpect(jsonPath("$.bulanLogAsisten").value("Oktober"))
                .andExpect(jsonPath("$.tahunLogAsisten").value(2021));
    }

    @PutMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<LogAsisten> updateLogAsisten(@PathVariable(value = "id") int id,
                                                       @RequestBody LogAsisten logAsisten) {
        return ResponseEntity.ok(logAsistenService.updateLogAsisten(id, logAsisten));
    }

    @Test
    void testUpdateLogAsisten() throws Exception {
        when(logAsistenService.updateLogAsisten(0, logAsisten2)).thenReturn(logAsisten1);

        mockMvc.perform(put("/api/log-asisten/0")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(logAsistenDTO2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0))
                .andExpect(jsonPath("$.tanggalLogAsisten").value(24))
                .andExpect(jsonPath("$.bulanLogAsisten").value("Oktober"))
                .andExpect(jsonPath("$.tahunLogAsisten").value(2021));
    }

    @Test
    void testDeleteLogAsisten() throws Exception {
        when(logAsistenService.deleteLogAsisten(0)).thenReturn(logAsisten1);

        mockMvc.perform(delete("/api/log-asisten/0")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(logAsisten2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0))
                .andExpect(jsonPath("$.tanggalLogAsisten").value(24))
                .andExpect(jsonPath("$.bulanLogAsisten").value("Oktober"))
                .andExpect(jsonPath("$.tahunLogAsisten").value(2021));
    }
}
