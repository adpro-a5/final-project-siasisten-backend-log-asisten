package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.StatusLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service.LogAsistenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = StatusLogAsistenApiController.class)
class StatusLogAsistenApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LogAsistenService logAsistenService;

    private List<LogAsisten> logAsistenList;
    private LogAsisten logAsisten1;


    @BeforeEach
    void setUp() {
        logAsistenList = new ArrayList<>();
        logAsisten1 = new LogAsisten("0", "0", 24, "Oktober",
                2021, 0, 0, 0,
                1, 1, 1, "Asistensi",
                "UTS");
        logAsisten1.setId(0);
        LogAsisten logAsisten2 = new LogAsisten("1", "0", 24, "Oktober",
                2021, 0, 0, 0,
                1, 0, 0, "Asistensi",
                "UTS");
        logAsisten2.setId(1);

        logAsistenList.add(logAsisten1);
        logAsistenList.add(logAsisten2);

    }

    @Test
    void testGetLogAsistenByIdMataKuliah() throws Exception {
        when(logAsistenService.getAllLogAsistenByIdMataKuliah("0", "0"))
                .thenReturn(logAsistenList);

        mockMvc.perform(get("/api/status-log-asisten/0")
                        .contentType(MediaType.APPLICATION_JSON).param("npmMahasiswa", "0"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].tanggalLogAsisten").value(24))
                .andExpect(jsonPath("$[0].bulanLogAsisten").value("Oktober"))
                .andExpect(jsonPath("$[0].tahunLogAsisten").value(2021));
    }

    @Test
    void testUpdateStatusLog() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("statusLogAsisten", "DIPROSES");
        logAsisten1.setStatusLogAsisten(StatusLogAsisten.DIPROSES);

        when(logAsistenService.updateStatusLogAsisten(0, "DIPROSES")).thenReturn(logAsisten1);
        mockMvc.perform(put("/api/status-log-asisten/0")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(map)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0))
                .andExpect(jsonPath("$.tanggalLogAsisten").value(24))
                .andExpect(jsonPath("$.bulanLogAsisten").value("Oktober"))
                .andExpect(jsonPath("$.tahunLogAsisten").value(2021))
                .andExpect(jsonPath("$.statusLogAsisten").value("DIPROSES"));
    }

}
