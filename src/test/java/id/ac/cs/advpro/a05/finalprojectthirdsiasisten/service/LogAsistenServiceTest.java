package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times.ToHours;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times.ToMinutes;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.times.ToSeconds;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LogAsistenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LogAsistenServiceTest {
    Class<?> logAsistenServiceClass;

    @Mock
    private LogAsistenRepository logAsistenRepository;

    @Mock
    private LaporanLogAsistenService laporanLogAsistenService;

    @InjectMocks
    private LogAsistenServiceImpl logAsistenServiceImpl;

    private List<LogAsisten> logAsistenList;
    private LogAsisten logAsisten1;
    private LogAsisten logAsisten2;
    private LogAsisten logAsisten3;

    @BeforeEach
    void setup() throws Exception {
        logAsistenServiceClass = Class.forName("id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service.LogAsistenService");
        logAsistenList = new ArrayList<>();
        logAsisten1 = new LogAsisten("0", "0", 24, "Oktober",
                2021, 0, 0, 0,
                1, 1, 1, "Asistensi",
                "UTS");
        logAsisten2 = new LogAsisten("1", "0", 24, "Oktober",
                2021, 0, 0, 0,
                1, 0, 0, "Asistensi",
                "UTS");
        logAsisten3 = new LogAsisten("1", "0", 24, "Oktober",
                2021, 0, 0, 0,
                1, 1, 0, "Asistensi",
                "UTS");
        logAsistenList.add(logAsisten1);
        logAsistenList.add(logAsisten2);
        ToHours toHours = ToHours.getInstance();
        ToMinutes toMinutes = ToMinutes.getInstance();
        ToSeconds toSeconds = ToSeconds.getInstance();
        toHours.setNextTimes(toMinutes);
        toMinutes.setNextTimes(toSeconds);
    }

    @Test
    void testLogAsistenServiceHasGetAllLogAsistenMethod() throws Exception {
        Method getAllLogAsisten = logAsistenServiceClass.getDeclaredMethod(
                "getAllLogAsisten",
                String.class);
        int methodModifiers = getAllLogAsisten.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getAllLogAsisten.getParameterCount());
    }

    @Test
    void testGetAllLogAsisten() {
        when(logAsistenRepository
                .findAllByNpmMahasiswaOrderByTanggalLogAsistenAscBulanLogAsistenAscTahunLogAsistenAsc("0"))
                .thenReturn(logAsistenList);
        assertEquals(logAsistenList, logAsistenServiceImpl.getAllLogAsisten("0"));
    }

    @Test
    void testLogAsistenServiceHasGetAllLogAsistenByIdMataKuliahMethod() throws Exception {
        Method getAllLogAsistenByIdMataKuliah = logAsistenServiceClass.getDeclaredMethod(
                "getAllLogAsistenByIdMataKuliah",
                String.class, String.class);
        int methodModifiers = getAllLogAsistenByIdMataKuliah.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, getAllLogAsistenByIdMataKuliah.getParameterCount());
    }

    @Test
    void testGetAllLogAsistenByIdMataKuliah() {
        List<LogAsisten> logAsistens = new ArrayList<>();
        logAsistens.add(logAsistenList.get(0));
        when(logAsistenRepository
                .findAllByIdMataKuliahAndNpmMahasiswaOrderByTanggalLogAsistenAscBulanLogAsistenAscTahunLogAsistenAsc
                        ("0", "0")).thenReturn(logAsistens);
        assertEquals(logAsistens, logAsistenServiceImpl.getAllLogAsistenByIdMataKuliah("0", "0"));
    }

    @Test
    void testLogAsistenServiceHasCreateLogAsistenMethod() throws Exception {
        Method createLogAsisten = logAsistenServiceClass.getDeclaredMethod(
                "createLogAsisten",
                LogAsisten.class);
        int methodModifiers = createLogAsisten.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, createLogAsisten.getParameterCount());
    }

    @Test
    void testCreateLogAsisten() {
        logAsistenServiceImpl.createLogAsisten(logAsisten1);
        assertEquals(1, logAsisten1.getDetikTotalWaktuLogAsisten());
        assertEquals(1, logAsisten1.getMenitTotalWaktuLogAsisten());
        assertEquals(1, logAsisten1.getJamTotalWaktuLogAsisten());
        verify(laporanLogAsistenService, times(1))
                .updateLaporanLogAsistenWithLog(logAsisten1, logAsisten1, "create");

        logAsistenServiceImpl.createLogAsisten(logAsisten2);
        assertEquals(0, logAsisten2.getDetikTotalWaktuLogAsisten());
        assertEquals(0, logAsisten2.getMenitTotalWaktuLogAsisten());
        assertEquals(1, logAsisten2.getJamTotalWaktuLogAsisten());
        verify(laporanLogAsistenService, times(1))
                .updateLaporanLogAsistenWithLog(logAsisten2, logAsisten2, "create");
    }

    @Test
    void testLogAsistenServiceHasGetLogAsistenByIdMethod() throws Exception {
        Method getLogAsistenById = logAsistenServiceClass.getDeclaredMethod(
                "getLogAsistenById",
                int.class);
        int methodModifiers = getLogAsistenById.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getLogAsistenById.getParameterCount());
    }

    @Test
    void testGetLogAsistenById() {
        assertEquals(logAsistenRepository.findLogAsistenById(0), logAsistenServiceImpl.getLogAsistenById(0));
    }

    @Test
    void testLogAsistenServiceHasUpdateLogAsistenMethod() throws Exception {
        Method updateLogAsisten = logAsistenServiceClass.getDeclaredMethod(
                "updateLogAsisten",
                int.class, LogAsisten.class);
        int methodModifiers = updateLogAsisten.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, updateLogAsisten.getParameterCount());
    }

    @Test
    void testUpdateLogAsisten() {
        when(logAsistenRepository.findLogAsistenById(0)).thenReturn(logAsisten1);
        logAsistenServiceImpl.updateLogAsisten(0, logAsisten3);
        assertEquals(0, logAsisten3.getDetikTotalWaktuLogAsisten());
        assertEquals(1, logAsisten3.getMenitTotalWaktuLogAsisten());
        assertEquals(1, logAsisten3.getJamTotalWaktuLogAsisten());
        verify(laporanLogAsistenService, times(1))
                .updateLaporanLogAsistenWithLog(logAsisten1, logAsisten3, "update");
    }

    @Test
    void testLogAsistenServiceHasDeleteLogAsistenMethod() throws Exception {
        Method deleteLogAsisten = logAsistenServiceClass.getDeclaredMethod(
                "deleteLogAsisten",
                int.class);
        int methodModifiers = deleteLogAsisten.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, deleteLogAsisten.getParameterCount());
    }

    @Test
    void testDeleteLogAsisten() {
        when(logAsistenRepository.findLogAsistenById(0)).thenReturn(logAsisten1);
        logAsistenServiceImpl.deleteLogAsisten(0);
        verify(logAsistenRepository, times(1)).delete(logAsisten1);
        verify(laporanLogAsistenService, times(1))
                .updateLaporanLogAsistenWithLog(logAsisten1, logAsisten1, "delete");
    }

    @Test
    void testUpdateStatusLogAsisten() {
        when(logAsistenRepository.findLogAsistenById(0)).thenReturn(logAsisten1);
        logAsistenServiceImpl.updateStatusLogAsisten(0, "DIPROSES");
        verify(logAsistenRepository, times(1))
                .delete(logAsisten1);
    }

}
