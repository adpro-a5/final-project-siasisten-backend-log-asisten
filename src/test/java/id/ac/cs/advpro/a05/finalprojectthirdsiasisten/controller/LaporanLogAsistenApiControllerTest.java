package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LaporanLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.StatusLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service.LaporanLogAsistenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LaporanLogAsistenApiController.class)
class LaporanLogAsistenApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LaporanLogAsistenService laporanLogAsistenService;

    private List<LaporanLogAsisten> allList;

    @BeforeEach
    void setUp() {
        allList = new ArrayList<>();
        LaporanLogAsisten logAsisten1 = new LaporanLogAsisten("0", "0",
                "Oktober", 2021, 25000, StatusLogAsisten.DILAPORKAN);
        LaporanLogAsisten logAsisten2 = new LaporanLogAsisten("0", "0",
                "Oktober", 2021, 25000, StatusLogAsisten.SELESAI);
        allList.add(logAsisten1);
        allList.add(logAsisten2);
    }

    @Test
    void testGetLaporanLogAsisten() throws Exception {
        when(laporanLogAsistenService.getAllLaporanLogSiasistenByNpmBulanTahun(
                "0", "Oktober", 2021)).thenReturn(allList);

        mockMvc.perform(get("/api/laporan-log-asisten")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("npmMahasiswa", "0")
                        .param("bulanLaporan", "Oktober")
                        .param("tahunLaporan", String.valueOf(2021)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].idMataKuliah").value("0"))
                .andExpect(jsonPath("$[0].npmMahasiswa").value("0"))
                .andExpect(jsonPath("$[0].bulanLaporanLogAsisten").value("Oktober"))
                .andExpect(jsonPath("$[0].tahunLaporanLogAsisten").value(2021));
    }

    @Test
    void testGetLaporanLogAsistenByStatus() throws Exception {
        when(laporanLogAsistenService.getAllLaporanLogSiasistenByNpmBulanTahunStatus(
                "0", "Oktober", 2021, StatusLogAsisten.DILAPORKAN)).thenReturn(allList);

        mockMvc.perform(get("/api/laporan-log-asisten/DILAPORKAN")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("npmMahasiswa", "0")
                        .param("bulanLaporan", "Oktober")
                        .param("tahunLaporan", String.valueOf(2021)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].idMataKuliah").value("0"))
                .andExpect(jsonPath("$[0].npmMahasiswa").value("0"))
                .andExpect(jsonPath("$[0].bulanLaporanLogAsisten").value("Oktober"))
                .andExpect(jsonPath("$[0].tahunLaporanLogAsisten").value(2021));
    }

}
