package id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service;

import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten.Create;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten.Delete;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.core.logasisten.Update;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LaporanLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.LogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.model.StatusLogAsisten;
import id.ac.cs.advpro.a05.finalprojectthirdsiasisten.repository.LaporanLogAsistenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class LaporanLogAsistenServiceTest {
    Class<?> laporanLogAsistenServiceClass;

    @Mock
    private LaporanLogAsistenRepository laporanLogAsistenRepository;

    @InjectMocks
    private LaporanLogAsistenServiceImpl laporanLogAsistenService;

    private LaporanLogAsisten laporanLogAsistenJanuari1;
    private LaporanLogAsisten laporanLogAsistenJanuari6;
    private List<LaporanLogAsisten> laporanLogAsistenList;
    private List<LaporanLogAsisten> laporanLogAsistenListDilaporkan;
    private LogAsisten logAsisten1;
    private LogAsisten logAsisten2;
    private LogAsisten logAsisten3;

    @BeforeEach
    void setup() throws Exception {
        laporanLogAsistenServiceClass = Class.forName(
                "id.ac.cs.advpro.a05.finalprojectthirdsiasisten.service.LaporanLogAsistenService");
        laporanLogAsistenJanuari1 = new LaporanLogAsisten("0", "0",
                "Januari", 2022, 25000, StatusLogAsisten.DILAPORKAN);
        LaporanLogAsisten laporanLogAsistenJanuari2 = new LaporanLogAsisten("0", "0",
                "Januari", 2022, 25000, StatusLogAsisten.DISETUJUI);
        LaporanLogAsisten laporanLogAsistenJanuari3 = new LaporanLogAsisten("0", "0",
                "Januari", 2022, 25000, StatusLogAsisten.TIDAK_DISETUJUI);
        LaporanLogAsisten laporanLogAsistenJanuari4 = new LaporanLogAsisten("0", "0",
                "Januari", 2022, 25000, StatusLogAsisten.DIPROSES);
        LaporanLogAsisten laporanLogAsistenJanuari5 = new LaporanLogAsisten("0", "0",
                "Januari", 2022, 25000, StatusLogAsisten.SELESAI);
        laporanLogAsistenJanuari6 = new LaporanLogAsisten("1", "0",
                "Januari", 2022, 25000, StatusLogAsisten.DILAPORKAN);

        laporanLogAsistenList = new ArrayList<>();
        laporanLogAsistenList.add(laporanLogAsistenJanuari1);
        laporanLogAsistenList.add(laporanLogAsistenJanuari2);
        laporanLogAsistenList.add(laporanLogAsistenJanuari3);
        laporanLogAsistenList.add(laporanLogAsistenJanuari4);
        laporanLogAsistenList.add(laporanLogAsistenJanuari5);

        laporanLogAsistenListDilaporkan = new ArrayList<>();
        laporanLogAsistenListDilaporkan.add(laporanLogAsistenJanuari1);
        laporanLogAsistenListDilaporkan.add(laporanLogAsistenJanuari6);

        Create create = Create.getInstance();
        Update update = Update.getInstance();
        Delete delete = Delete.getInstance();
        create.setNext(update);
        update.setNext(delete);

        logAsisten1 = new LogAsisten("0", "0", 24, "Januari",
                2021, 0, 0, 0,
                1, 1, 1, "Asistensi",
                "UTS");
        logAsisten2 = new LogAsisten("0", "0", 24, "Januari",
                2021, 0, 0, 0,
                1, 0, 0, "Asistensi",
                "UTS");
        logAsisten3 = new LogAsisten("1", "0", 24, "Januari",
                2021, 0, 0, 0,
                1, 1, 0, "Asistensi",
                "UTS");
    }

    @Test
    void testLaporanLogAsistenServiceHasGetAllLogAsistenMethod() throws Exception {
        Method getAllLaporanLogSiasistenByNpmBulanTahun = laporanLogAsistenServiceClass.getDeclaredMethod(
                "getAllLaporanLogSiasistenByNpmBulanTahun",
                String.class, String.class, int.class);
        int methodModifiers = getAllLaporanLogSiasistenByNpmBulanTahun.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(3, getAllLaporanLogSiasistenByNpmBulanTahun.getParameterCount());
    }

    @Test
    void testGetAllLogAsisten() {
        when(laporanLogAsistenRepository
                .findAllByNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenOrderByIdMataKuliah
                        ("0", "Januari", 2022))
                .thenReturn(laporanLogAsistenList);

        assertEquals(
                laporanLogAsistenRepository
                        .findAllByNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenOrderByIdMataKuliah
                                ("0", "Januari", 2022),
                laporanLogAsistenService
                        .getAllLaporanLogSiasistenByNpmBulanTahun
                                ("0", "Januari", 2022));
    }

    @Test
    void testLaporanLogAsistenServiceHasGetAllLogAsistenByStatusMethod() throws Exception {
        Method getAllLaporanLogSiasistenByNpmBulanTahunStatus = laporanLogAsistenServiceClass.getDeclaredMethod(
                "getAllLaporanLogSiasistenByNpmBulanTahunStatus",
                String.class, String.class, int.class, StatusLogAsisten.class);
        int methodModifiers = getAllLaporanLogSiasistenByNpmBulanTahunStatus.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(4, getAllLaporanLogSiasistenByNpmBulanTahunStatus.getParameterCount());
    }

    @Test
    void testGetAllLogAsistenByStatus() {
        when(laporanLogAsistenRepository
                .findAllByNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                        ("0", "Januari", 2022, StatusLogAsisten.DILAPORKAN))
                .thenReturn(laporanLogAsistenListDilaporkan);

        assertEquals(
                laporanLogAsistenRepository
                        .findAllByNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                                ("0", "Januari", 2022, StatusLogAsisten.DILAPORKAN),
                laporanLogAsistenService
                        .getAllLaporanLogSiasistenByNpmBulanTahunStatus
                                ("0", "Januari", 2022, StatusLogAsisten.DILAPORKAN));
    }

    @Test
    void testLaporanLogAsistenServiceHasUpdateLaporanLogAsistenWithCreateLogMethod() throws Exception {
        Method updateLaporanLogAsistenWithLog = laporanLogAsistenServiceClass.getDeclaredMethod(
                "updateLaporanLogAsistenWithLog",
                LogAsisten.class, LogAsisten.class, String.class);
        int methodModifiers = updateLaporanLogAsistenWithLog.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(3, updateLaporanLogAsistenWithLog.getParameterCount());
    }

    @Test
    void testUpdateLaporanLogAsistenWithCreateLog() {
        when(laporanLogAsistenRepository
                .findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                        (logAsisten1.getIdMataKuliah(), logAsisten1.getNpmMahasiswa(), logAsisten1.getBulanLogAsisten(),
                                logAsisten1.getTahunLogAsisten(), logAsisten1.getStatusLogAsisten()))
                .thenReturn(laporanLogAsistenJanuari1);

        laporanLogAsistenService.updateLaporanLogAsistenWithLog(logAsisten1, logAsisten1, "create");
        verify(laporanLogAsistenRepository, atLeast(1)).findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                (logAsisten1.getIdMataKuliah(), logAsisten1.getNpmMahasiswa(), logAsisten1.getBulanLogAsisten(),
                        logAsisten1.getTahunLogAsisten(), logAsisten1.getStatusLogAsisten());
    }

    @Test
    void testUpdateLaporanLogAsistenWithUpdateLog() {
        when(laporanLogAsistenRepository
                .findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                        (logAsisten2.getIdMataKuliah(), logAsisten2.getNpmMahasiswa(), logAsisten2.getBulanLogAsisten(),
                                logAsisten2.getTahunLogAsisten(), logAsisten2.getStatusLogAsisten()))
                .thenReturn(laporanLogAsistenJanuari1);

        laporanLogAsistenService.updateLaporanLogAsistenWithLog(logAsisten1, logAsisten2, "update");
        verify(laporanLogAsistenRepository, atLeast(1)).findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                (logAsisten1.getIdMataKuliah(), logAsisten1.getNpmMahasiswa(), logAsisten1.getBulanLogAsisten(),
                        logAsisten1.getTahunLogAsisten(), logAsisten1.getStatusLogAsisten());
    }

    @Test
    void testUpdateLaporanLogAsistenWithDelete() {
        when(laporanLogAsistenRepository
                .findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                        (logAsisten3.getIdMataKuliah(), logAsisten3.getNpmMahasiswa(), logAsisten3.getBulanLogAsisten(),
                                logAsisten3.getTahunLogAsisten(), logAsisten3.getStatusLogAsisten()))
                .thenReturn(laporanLogAsistenJanuari6);

        laporanLogAsistenService.updateLaporanLogAsistenWithLog(logAsisten3, logAsisten3, "delete");
        verify(laporanLogAsistenRepository, atLeast(1)).findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                (logAsisten3.getIdMataKuliah(), logAsisten3.getNpmMahasiswa(), logAsisten3.getBulanLogAsisten(),
                        logAsisten3.getTahunLogAsisten(), logAsisten3.getStatusLogAsisten());
    }

    @Test
    void testUpdateLaporanLogAsistenNull() {
        when(laporanLogAsistenRepository
                .findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                        (logAsisten3.getIdMataKuliah(), logAsisten3.getNpmMahasiswa(), logAsisten3.getBulanLogAsisten(),
                                logAsisten3.getTahunLogAsisten(), logAsisten3.getStatusLogAsisten()))
                .thenReturn(null);

        laporanLogAsistenService.updateLaporanLogAsistenWithLog(logAsisten3, logAsisten3, "action");
        verify(laporanLogAsistenRepository, atLeast(1)).findByIdMataKuliahAndNpmMahasiswaAndBulanLaporanLogAsistenAndTahunLaporanLogAsistenAndStatusLogAsisten
                (logAsisten3.getIdMataKuliah(), logAsisten3.getNpmMahasiswa(), logAsisten3.getBulanLogAsisten(),
                        logAsisten3.getTahunLogAsisten(), logAsisten3.getStatusLogAsisten());
    }
}
